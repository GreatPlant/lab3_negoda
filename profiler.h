#pragma once
#include <sstream>
#include <fstream>
#include <iomanip>
#include <cmath>
#include "vpv_lab3.h"

// Тип многопоточной функции, измеряемой в Tester.measure()
typedef double (*FuncThreadGranul)(int threads, int granul);
// Накопление данных, фильтрация и статистическая обработка
class Log {
public:
   std::vector<double> val;// протокол серии измерений
   ResultOfSeries res;
   //int thread, granularity; // число потоков и гранулярность
   int count;			// длина серии измерений
   int delMax, delMin; // число удаляемых максимальных и минимальных элементов
   void calc() {
      std::vector<double> fval = val; // фильтруемый массив серии измерений
      if (fval.size() > 0) {
         sort(fval.begin(), fval.end());
         // фильтрация через уд2аление максимальных и минимальных значений
         for (auto n = delMax; n > 0 && fval.size() > 1; n--)
            fval.pop_back();
         for (auto n = delMin; n > 0 && fval.size() > 1; n--)
            fval.erase(fval.begin());
         res.min = *fval.begin();
         res.max = *(fval.end() - 1);
         auto sz = fval.size();
         // Вычисление суммы и среднего
         double sum = 0;
         for (double el: fval)
            sum += el;
         res.avg = sum / sz;
         // Вычисление СКО
         double dev = 0.;
         for (double el : fval) {
            double diff = (double)el - res.avg;
            dev += diff * diff;
         }
         dev = std::sqrt(dev/sz); // СКО
         res.dev = (res.avg < 1.E-200)? 0. : (100 * dev) / res.avg; // СКО%
      }
   }
};

// Базовый класс верификации - замера времени
class Tester {
public:
    std::string name;		// Семантическое наименование метода реализации функции
    int numFunc;		// Индекс функции с параллельной обработкой
    double (* calcParallel)(int, int); // Адрес тестируемой функции
    double maxErr;		// Предельно допустимая ошибка
    Log log;			// Протокол conf.count результатов измерений с функциями статистической обработки
    Report* report;	// Адрес отчета, куда передается результат
    int64_t overhead;   // накладные расходы на измерение
    double etalon;		// эталонное значение интеграла для верификации
    bool proper;		// Признак успешности функционального тестирования
    Tester(std::string nm, double (* func) (int, int), Config & conf, int nFunc)
        : name(nm), numFunc(nFunc), calcParallel(func), maxErr(conf.maxErr), report(conf.rep),  etalon(conf.etalon){
        log.count = conf.count;
        log.delMax = conf.delMax;
        log.delMin = 0;
        proper = false;
    }

    // Вычисление ошибки выполняется для максимально возможного числа потоков и минимальноно числа подинтервалов
    std::string calcError() {
        std::ostringstream str;
        double err;
        str << name;
        err = calcParallel(4, 100) - etalon;
        if (std::abs(err) > maxErr) {
                str << ": Ошибка = " << err;
                proper = false;
                return str.str();
            }
        proper = true;
        str << " - OK";
        return str.str();
    }
    void verify() { std::cout << calcError() << std::endl; }
    void measure() { // Замеры времени для repeat значений x in [0, 1) с равномерным шагом
        double t; // засечка времени
        double res; // результат вычисления для подсчета ошибок
        std::cout << std::endl << name;
        for (int threads = 1; threads <= 12; threads++) {
            std::cout << std::endl << std::setw(WIDTH_COL) << threads;
            for (int gran = 0; gran < COUNT_GRANULARITY; gran++) {
                // Серия log.count измерений
                int subIntervals = report->arrGranularity[gran];
                log.res.threads = threads;
                log.res.granularity = subIntervals;
                log.val.clear();
                for (int n = 0; n < log.count; n++) {
                    t = omp_get_wtime();
                    res = calcParallel(threads, subIntervals);
                    log.val.push_back(omp_get_wtime() - t);
                } // серия
                log.res.error = res - etalon;
                log.calc();
                report->cubeResults[numFunc].name = name;
                report->cubeResults[numFunc].matrix[threads-1][gran] = log.res;
                std::cout << std::setw(WIDTH_COL) << std::fixed << std::setprecision(1) << MICROSEC(log.res.min);
            } // гранулярности
        } // потоки
    }
};

