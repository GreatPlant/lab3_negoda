TEMPLATE = app
CONFIG -= app_bundle
CONFIG -= qt

CONFIG += console

SOURCES += \
    integral.cpp \
    vpv_lab3.cpp

HEADERS += \
    vpv_lab3.h \
    integral.h \
    init.h \
    histogram.h \
    profiler.h \
    report.h

QMAKE_CXXFLAGS += -std=gnu++0x -O3 -pthread -fopenmp
LIBS += -pthread -fopenmp
