#include "vpv_lab3.h"
#include "report.h"
#include "profiler.h"
#include "init.h"
#include "integral.h"

int main(int argc, char * argv[])
{
    Report report = Report();
    Config config(
        50,     // число замеров в серии измерений
        10,     // число удаляемых максимумов во время фильтрации результатов серии
        &report, // адрес отчета, куда складываются результаты измерений
        MAX_ERR, // максимально допустимая погрешность вычислений
        0.0     // эталонное значение интеграла для верификатора (вычисляется в init())
    );
    init(argc, argv, config, report);
    // Создание массива объектов тsестирования
    Tester tests[COUNT_FUNCTION] = {
        Tester("integralThread", integralThread, config, 0),
        Tester("integralThreadStatic", integralThreadStatic, config, 1),
        Tester("integralThreadStaticNoLambda", integralThreadStaticNoLambda, config, 2),
        Tester("integralOpenMP", integralOpenMP, config, 3)
    };
    std::cout << std::endl << "Функциональное тестирование ..." << std::endl;
    for (Tester test : tests)
        test.verify();
    std::cout << std::endl << "Замеры времени(микросекунды) ..." << std::endl;
    // Цикл поддержки анализа повторяемости
    do {
        for (Tester test : tests)
            test.measure();
    } while (report.show());
    return 0;
}

