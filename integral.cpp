#include "integral.h"

// Определенный интеграл функции sin(x)/x на интервале 0..1 через длинный ряд
// f(x) = sin(x) / x = 1 - x^2/3! + x^4/5! - x^6/7! + ... + (-1)^n * x^(2n+1) / (2n+1)! ...
// Первообразная(x) = F(x) = x - x^3 / (3*3!) + x^5 / (5*5!) -x^7/(7*7!) + ... + (-1)^n * x^(2n+1) / ((2n+1)*(2n+1)!) + ...
// Интеграл(0, 1) = F(1) - F(0) = 1 - 1/(3*3!) + 1 / (5*5!) -1/(7*7!) + ... + (-1)^n / ((2n+1)*(2n+1)!)
//double calcEtalon () {
//    double	sum = 1.0, // сумма членов степенного ряда
//            fact = 1.0; // факториал тоже double, чтобы избежать переполнения, поскольку uint64 поместит только 20!
//    int sign = 1;
//    for (int n = 1; n <= 20; n++) {
//        fact *= 2 * n * (2*n + 1);
//        sign *= -1;
//        sum += sign / ((2 * n + 1) * fact);
//    }
//    return sum;
//}

auto sumTrapeze = [](int nRect, int thRect, double wRect) {
    double x, sum = 0.0;
    thRect +=  nRect;
    // цикл суммирования высот средних прямоугольников
    do {
        x = nRect * wRect + 0.5 * wRect;
        sum += std::log(1 - x);
    } while (++nRect < thRect);
    // подсуммирование результата к глобальной сумме
    mu.lock();
    globSum = globSum + sum;
    mu.unlock();
};

double integralThread(int nThreads, int allRect) {
    double wRect = 1.0 / (double)allRect; // ширина прямоугольника численного метода
    // число прямоугольников для одного потока
    int thRect = (allRect - 1) / nThreads + 1;
    // корректировка числа потоков в случае, когда их больше, чем allRect
    nThreads = (allRect - 1) / thRect + 1;
    int restRect = allRect - (nThreads - 1) * thRect;
    if (restRect == 0) restRect = thRect; // когда allRect кратно nThreads
    globSum = 0.0;
    // Создаем nThreads потоков
    thread * threads = new thread[nThreads];
    for (int n = 0; n < nThreads; n++)
        threads[n] = thread(sumTrapeze, n * thRect, n < nThreads - 1? thRect : restRect, wRect);
    for (int n = 0; n < nThreads; n++) threads[n].join();
    return globSum * wRect; // возврат площади всех средних прямоугольников
}

double integralThreadStatic(int nThreads, int allRect) {
    double wRect = 1.0 / (double)allRect; // ширина прямоугольника численного метода
// число прямоугольников для одного потока
    int thRect = (allRect - 1) / nThreads + 1;
    // корректировка числа потоков в случае, когда их больше, чем allRect
    nThreads = (allRect - 1) / thRect + 1;
    int restRect = allRect - (nThreads - 1) * thRect;
    if (restRect == 0) restRect = thRect; // когда allRect кратно nThreads
    globSum = 0.0;
    // Создаем nThreads потоков
    for (int n = 0; n < nThreads; n++)
        threadsStatic[n] = thread(sumTrapeze, n * thRect, n < nThreads - 1 ? thRect : restRect, wRect);
    for (int n = 0; n < nThreads; n++)
        threadsStatic[n].join();
    return globSum * wRect; // возврат площади всех средних прямоугольников
}

// Функция суммирования высот прямоугольников,  с шириной wRect
void fuSum(int nRect, int thRect, double wRect) {
    double x, sum = 0.0;
    thRect += nRect;
    // цикл суммирования высот средних прямоугольников
    do {
        x = nRect * wRect + 0.5 * wRect;
        sum += std::log(1 - x);
    } while (++nRect < thRect);
    mu.lock();
    globSum = globSum + sum;
    mu.unlock();

};

double integralThreadStaticNoLambda(int nThreads, int allRect) {
    double wRect = 1.0 / (double)allRect; // ширина прямоугольника численного метода
// число прямоугольников для одного потока
    int thRect = (allRect - 1) / nThreads + 1;
    // корректировка числа потоков в случае, когда их больше, чем allRect
    nThreads = (allRect - 1) / thRect + 1;
    int restRect = allRect - (nThreads - 1) * thRect;
    if (restRect == 0) restRect = thRect; // когда allRect кратно nThreads
    globSum = 0.0;
    // Создаем nThreads потоков
    for (int n = 0; n < nThreads; n++)
        threadsStatic[n] = thread(fuSum, n * thRect, n < nThreads - 1 ? thRect : restRect, wRect);
    for (int n = 0; n < nThreads; n++)
        threadsStatic[n].join();
    return globSum * wRect; // возврат площади всех средних прямоугольников
}

// Вычисление интеграла через библиотеку openMP
//   numTreads потоками с разбиением интервала 0..1 на numSubIntervals подинтервалов
double integralOpenMP(int numTreads, int numSubIntervals) {
    int i;
    double x;
    double sum = 0.0;
    double wRect = 1.0 / (double)numSubIntervals;
    omp_set_num_threads(numTreads);
#pragma omp parallel for private(x) reduction(+:sum)
    for (i = 0; i < numSubIntervals; i++)
    {	x = i * wRect + wRect / 2;
        sum += std::log(1 - x);
    }
    return sum * wRect;
}
