#pragma once
#include <map>
#include <string>
#include <iostream>
#include <string.h>
#include <stdint.h>
#include <x86intrin.h>
#include <cpuid.h>
#include "report.h"
#include "integral.h"
#include "vpv_lab3.h"

using namespace std;

using TOpt = std::map<std::string, std::string>;

TOpt makeOpsMap(int argc, char *argv[]) {
    TOpt ops;
    char *pcolon;
    while (argc > 0) {
        char * parg = argv[--argc];
        if ((pcolon = strchr(parg, ':')) != NULL) {
            *pcolon = '\0';
            ops.insert({ parg, pcolon + 1 });
        }
    }
    return ops;
}
static inline void native_cpuid(unsigned int *eax, unsigned int *ebx,
                                unsigned int *ecx, unsigned int *edx)
{
        /* ecx is often an input as well as an output. */
        asm volatile("cpuid"
            : "=a" (*eax),
              "=b" (*ebx),
              "=c" (*ecx),
              "=d" (*edx)
            : "0" (*eax), "2" (*ecx));
}


void cpuInfo(Report& report) {
    unsigned reg[4] = { 1, 0, 0, 0 };
    char CPUName[80];
    native_cpuid(&reg[0], &reg[1], &reg[2], &reg[3]);
//    __cpuid(0, reg[0], reg[1], reg[2], reg[3]);
    memcpy(CPUName, &reg[0], sizeof(unsigned));
    memcpy(CPUName + 16, &reg[1], sizeof(unsigned));
    memcpy(CPUName + 32, &reg[2], sizeof(unsigned));
    report.startText << endl << CPUName << endl << endl
                     << "Число вычислительных узлов: " << omp_get_num_procs() << endl;
}
void init(int argc, char * argv[], Config &conf, Report& report) {
    report.startText << "Высокопроизводительные вычисления. Лабораторная работа № 3."
        << endl << "Студент гр. ИВТВМбд-31 Горшков Л.И." << endl;
    TOpt::iterator it;
    setlocale(LC_CTYPE, "rus");

    TOpt ops = makeOpsMap(argc, argv);
    it = ops.find("slen");
    if (it != ops.end())
        conf.count = atoi(it->second.c_str());
    it = ops.find("dmax");
    if (it != ops.end())
        conf.delMax = atoi(it->second.c_str());
    conf.count = max(conf.count, conf.delMax + 3);
    conf.etalon = -1;
    cpuInfo(report);
    report.startText << "Размер серии: " << conf.count
        << ", отбрасываются " << conf.delMax << " наибольших" << '.'
        << endl << "Разрешение часов omp_wtime: " << (int) (omp_get_wtick() * 1.E9) << " ns" << endl
        << "Эталонное значение интеграла: " << conf.etalon
        << endl << "Единицы измерения времени: микросекунды" << endl;
    std::cout << report.startText.str();

}
