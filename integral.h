#pragma once
// *******************************************************************
// Параллельные реализации численного метода вычисления интеграла
// *******************************************************************
#include <thread>
#include <mutex>
#include <omp.h>
#include <math.h>
#include <time.h>
#include <stdio.h>
#include "vpv_lab3.h"


using namespace std;
static thread threadsStatic[MAX_THREAD];
static double globSum; // глобальная сумма высок прямоугольников от всех потоков
static mutex mu; // мьютекс для управления доступом к globSum
// Создание nTreads потоков с раскладкой по ним allRect прямоугольников
double integralThread(int, int);
// Определенный интеграл функции sin(x)/x на интервале 0..1 через длинный ряд
// f(x) = ln(1 - x ) = -x - x^2 / 2 - x^3 / 3 - ... - x^n / n - ...
// Первообразная(x) = F(x) =  -x^2 / 2 - x^3 / 6 - x^4 / 12 - ... - x^(n+1) / (n+1)n - ...
// Интеграл(0, 1) = F(1) - F(0) = -1/2 -1/6 - 1/12 - ... = 1 (доказано в отчете)
double calcEtalon ();

// Вычисление интеграла через библиотеку thread

// Создание nTreads потоков с раскладкой по ним allRect прямоугольников
double integralThread(int, int);

// Использование уже созданных nTreads потоков с раскладкой по ним allRect прямоугольников

double integralThreadStatic(int nThreads, int allRect);

// Функция суммирования высот прямоугольников,  с шириной wRect
void fuSum(int nRect, int thRect, double wRect);

double integralThreadStaticNoLambda(int nThreads, int allRect);

// Вычисление интеграла через библиотеку openMP
//   numTreads потоками с разбиением интервала 0..1 на numSubIntervals подинтервалов
double integralOpenMP(int numTreads, int numSubIntervals);
